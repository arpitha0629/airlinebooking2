package com.newtours.utilities;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

public class Screenshotfile 
{
		public static void takescreenshot(WebDriver driver,String filename) 
		{
			try {
			File srcfile=((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
			
			FileUtils.copyFile(srcfile, new File("D:\\Workspace\\airlinebooking\\Screenshots"+filename+".jpg"));
			
			
		
				
			} 
			
			catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
}
