package com.newtours.utilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.WebDriver;

import com.newtours.POM.login;

public class ReadExcel
{
	
	static String[][] str=null;
	static String Username=null;
	static String Password=null;
	
	static File src=null;
	static FileInputStream fis=null;
	static XSSFWorkbook wb=null;
	static XSSFSheet sheet1=null;
	
	
	
	public static String[][] readexcel() throws Exception
	{
		
		
		src=new File("D:\\Workspace\\airlinebooking\\testdata\\TestData.xlsx");
		
		fis=new FileInputStream(src);
			
		wb=new XSSFWorkbook(fis);
			
		sheet1=wb.getSheetAt(0);
					
		int rownum=sheet1.getLastRowNum();
		
		for(int i=0;i<=rownum;i++)
		{
			str[i][0]=sheet1.getRow(i).getCell(0).getStringCellValue(); 
			str[i][1]=sheet1.getRow(i).getCell(1).getStringCellValue(); 
		}
		
		
		return str;
		
		
		
	}



	
	
}
