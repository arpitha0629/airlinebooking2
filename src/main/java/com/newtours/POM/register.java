package com.newtours.POM;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class register 
{
	private static WebElement element=null;
	
	public static WebElement clickonregister(WebDriver driver)
	{
		element= driver.findElement(By.linkText("REGISTER"));
		
		return element;
	}
	
	public static void filldetails(WebDriver driver)
	{
		element= driver.findElement(By.xpath("//input[@name='firstName']"));
		element.sendKeys("RAJA");
		
		element=driver.findElement(By.xpath("//input[@name='lastName']"));
		element.sendKeys("RAJA");
		
		element=driver.findElement(By.xpath("//input[@name='phone']"));
		element.sendKeys("1234567890");
		
		
		element=driver.findElement(By.xpath("//input[@name='userName']"));
		element.sendKeys("raja@gmail.com");
		
		
		element=driver.findElement(By.xpath("//input[@name='address1']"));
		element.sendKeys("qwerty");
		
		
		element=driver.findElement(By.xpath("//input[@name='address2']"));
		element.sendKeys("qwerty");
		
		element=driver.findElement(By.xpath("//input[@name='city']"));
		element.sendKeys("Dallas");
		
		
		element=driver.findElement(By.xpath("//input[@name='state']"));
		element.sendKeys("Atlanta");
		
		
		element=driver.findElement(By.xpath("//input[@name='postalCode']"));
		element.sendKeys("12345");
		
		
		element=driver.findElement(By.xpath("//input[@name='email']"));
		element.sendKeys("raja");
		
		element=driver.findElement(By.xpath("//input[@name='password']"));
		element.sendKeys("raja123");
		
		element=driver.findElement(By.xpath("//input[@name='confirmPassword']"));
		element.sendKeys("raja123");
		
			
	}
	
	public static WebElement submit(WebDriver driver)
	{
		element= driver.findElement(By.xpath("//input[@name='register']"));
		
		return element;
	}
}
