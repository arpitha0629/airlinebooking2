package com.newtours.POM;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class login
{
	
	private static WebElement element=null;
	
	public static void givedata(WebDriver driver, String Username, String Password)
	{
		element=driver.findElement(By.xpath("//input[@name='userName']"));
		element.sendKeys(Username);
		
		
		element=driver.findElement(By.xpath("//input[@name='password']"));
		element.sendKeys(Password);
		
	}
	
	
	public static WebElement submitlogin(WebDriver driver)
	{
		element= driver.findElement(By.xpath("//input[@name='login']"));
		return element;
	}
	
}
