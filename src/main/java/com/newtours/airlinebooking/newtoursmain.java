package com.newtours.airlinebooking;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.newtours.POM.bookflight;
import com.newtours.POM.login;
import com.newtours.POM.logout;
import com.newtours.POM.register;
import com.newtours.utilities.ReadExcel;
import com.newtours.utilities.Screenshotfile;

public class newtoursmain {

	public static WebDriver driver;

	@BeforeMethod
	public void getsite() throws Exception {
		System.setProperty("webdriver.chrome.driver", "D:\\Workspace\\airlinebooking\\ChromeDriver\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://newtours.demoaut.com");
		Thread.sleep(5000);
	}

	
	  @Test(priority = 0)
	  public static void callregister()
	  {
	  
	  register.clickonregister(driver).click(); 
	  register.filldetails(driver);
	  register.submit(driver).click();
	  Screenshotfile.takescreenshot(driver, "Register_Page");
	  
	  }
	 

	
	  @Test(priority = 1) 
	  public static void calllogin() {
	  
	  String Username; String Password; 
	  try {
	  
	  String[][] str2=ReadExcel.readexcel();
	  
	  for(int i=0;i<str2.length;i++)
	  { 
		  Username=str2[i][0];
	 
	  Password=str2[i][1]; 
	  login.givedata(driver, Username, Password);
	  driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	  login.submitlogin(driver);
	 
	 } 
	  }
	  catch (Exception e) 
	  { e.getMessage(); } 
	  }
	 
	  
	 @Test(priority = 2)
	 public static void callbookingflight()
	 {
	 bookflight.login(driver);
	 bookflight.loginclick(driver).click();
	 bookflight.oneway(driver).click();
	  bookflight.deptfrom(driver); 
	  bookflight.deptmonth(driver);
	  bookflight.toport(driver);
	  bookflight.classtype(driver);
	  bookflight.continuebutton(driver).click();
	  bookflight.selectflight(driver).click();
	  bookflight.continuebutton2(driver).click(); 
	  // purchase
	  
	  bookflight.purchasepage(driver);
	  Screenshotfile.takescreenshot(driver, "Purchase_Page");
	  //logout
	  logout.clickonsignoff(driver).click();
	 }
	
	  
	
	@AfterMethod
	public void closesite() {
		driver.quit();
	}

}
